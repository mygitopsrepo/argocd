# Argo CD

## Introduction

Made for kubernetes specifically to deploy CD. The Argo CD itself is part of the cluster, it will pull the changes and then make changes to the cluster. 
When the commit is made, the pipelines are triggered the manifest files in the git repositories are changed which will intern be used to deploy the change made to the cluster. 

App source code and the configuration files are recommended to be stored seperately. Supports YAML Manifest file, Helm chart or anything with the ARGO CD

CI --> developer team for pipelines lke jenkins/gitlab and CD --> Operations and devops teams

One of the big benefits is the removal of the kubectl command. Argo CD will provide the simple and common interface accross systems to different developers working. Even if the changes were done manually, they will be synced with the git repository to get the actual state making it the only source of the truth. 

One Argo CD can be used to manage multiple clusters in different environments. One git repository will be managed for all the environments. This can be achieved by using the overlays with the kustomizations. 

Argo CD is a replacement for CD but not the CI and this can only be used on the kubernetes clusters. 

## Install Argo CD 
The installation is based on the Arco CD documentation. Getthe inital secret and decode it. To login using the CI port forward the service before logging in using the UI. 

Argo CD polls the repository every 3 min. For no delay, you can enable webhook integration.